data:extend({
  {
    type = "item",
    name = "rtg",
    icon = "__SigmaOne_Nuclear__/graphics/items/rtg.png",
    icon_size = 32,
    flags = {},
    subgroup = "energy",
    order = "g-a",
    place_result = "rtg",
    stack_size = 30
  },
})
